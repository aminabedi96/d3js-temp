getDataAndMakeChart = async ()=>{
    const config = await d3.json('config.json')
    const { orderOptions, keyAttr, valAttr, dataURI} = config
    // Create a drop down
    d3.select("#selection")
        .append("span")
        .append("select")
        .attr("id", "order")
        .attr("name", "tasks")
        .selectAll("option")
        .data(orderOptions)
        .enter()
        .append("option")
        .attr("value", d => d)
        .text(d => d)


    d3.select("#order")
        .on("change", function() {
            const selectedOption = d3.select(this).node().value;
            if (selectedOption == "Ascending") {
                chart.update((a,b) => {
                    return d3.ascending(a.value, b.value)
                }) 
            } else if (selectedOption == "Descending") {
            chart.update((a,b) => {
                return d3.descending(a.value, b.value)
            })
            } else if (selectedOption == "Alphabetical") {
            chart.update((a,b) => {
                return d3.ascending(a.letter, b.letter)
            })
            }
        })

    data = await d3.json(dataURI)
    data = data.map((record) => ({name: record[keyAttr], value: record[valAttr]}))
    chart =  makeChart(data, config)
    d3.select("#chart-container").append(()=>chart)
}

makeChart = (data, { height, width, margin, fillColor })=>{
    console.log({ height, width, margin, fillColor }, data)
    x = d3.scaleBand()
        .domain(data.map(d => d.name))
        .range([margin.left, width - margin.right])
        .padding(0.1)

    y = d3.scaleLinear()
        .domain([0, d3.max(data, d => d.value)]).nice()
        .range([height - margin.bottom, margin.top])
    console.log(y(0), y(0.1), y(.5), y(1), [0, d3.max(data, d => d.value)], [height - margin.bottom, margin.top])
    xAxis = g => g
        .attr("transform", `translate(0,${height - margin.bottom})`)
        .call(d3.axisBottom(x).tickSizeOuter(0))

    yAxis = g => g
        .attr("transform", `translate(${margin.left},0)`)
        .call(d3.axisLeft(y))
        .call(g => g.select(".domain").remove())

    const svg = d3.create("svg")
        .attr("viewBox", [0, 0, width, height]);

    const bar = svg.append("g")
        .attr("fill", fillColor)
        .selectAll("rect")
        .data(data)
        .join("rect")
        .style("mix-blend-mode", "multiply")
        .attr("x", d => x(d.name))
        .attr("y", d => y(d.value))
        .attr("height", d => y(0) - y(d.value))
        .attr("width", x.bandwidth());

    const gx = svg.append("g")
        .call(xAxis);

    const gy = svg.append("g")
        .call(yAxis);

    return Object.assign(svg.node(), {
        update(order) {
            x.domain(data.sort(order).map(d => d.name));

            const t = svg.transition()
                .duration(750);

            bar.data(data, d => d.name)
                .order()
            .transition(t)
                .delay((d, i) => i * 20)
                .attr("x", d => x(d.name));

            gx.transition(t)
                .call(xAxis)
            .selectAll(".tick")
                .delay((d, i) => i * 20);
        }
    });
}

getDataAndMakeChart()
