import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3'

type SequenceData = {
  name: string;
  value: number;
};

@Component({
  selector: 'app-sequence-sunburst',
  templateUrl: './sequence-sunburst.component.html',
  styleUrls: ['./sequence-sunburst.component.css']
})



export class SequenceSunburstComponent implements OnInit {

  
  private breadcrumbWidth: number;
  private breadcrumbHeight: number;
  private width: number;
  private radius: number;

  constructor() {
    this.breadcrumbWidth = 0
    this.breadcrumbHeight = 0
    this.width = 0
    this.radius = 0
  }

  private buildHierarchy(csv: any): any {
    // Helper function that transforms the given CSV into a hierarchical format.
    const root = { name: "root", children: [] };
    for (let i = 0; i < csv.length; i++) {
      const sequence = csv[i][0];
      const size = +csv[i][1];
      if (isNaN(size)) {
        // e.g. if this is a header row
        continue;
      }
      const parts = sequence.split("-");
      let currentNode: any = root;
      for (let j = 0; j < parts.length; j++) {
        const children = currentNode["children"];
        const nodeName = parts[j];
        let childNode = null;
        if (j + 1 < parts.length) {
          // Not yet at the end of the sequence; move down the tree.
          let foundChild = false;
          for (let k = 0; k < children.length; k++) {
            if (children[k]["name"] == nodeName) {
              childNode = children[k];
              foundChild = true;
              break;
            }
          }
          // If we don't already have a child node for this branch, create it.
          if (!foundChild) {
            childNode = { name: nodeName, children: [] };
            children.push(childNode);
          }
          currentNode = childNode;
        } else {
          // Reached the end of the sequence; create a leaf node.
          childNode = { name: nodeName, value: size };
          children.push(childNode);
        }
      }
    }
    return root;
  }
  private partition(data: SequenceData):any{    
    return d3.partition().size([2 * Math.PI, this.radius * this.radius])(
      d3
        .hierarchy(data)
        .sum(d => d.value)
        .sort((a:any, b:any) => b.value - a.value)
    )
  }

  private getBreadcrumbPoints() {
    const breadcrumbHeight = this.breadcrumbHeight
    const breadcrumbWidth = this.breadcrumbWidth
    return (d: any, i: any): string=>{
      const tipWidth = 10;
      const points = [];
      points.push("0,0");
      points.push(`${breadcrumbWidth},0`);
      points.push(`${breadcrumbWidth + tipWidth},${breadcrumbHeight / 2}`);
      points.push(`${breadcrumbWidth},${breadcrumbHeight}`);
      points.push(`0,${breadcrumbHeight}`);
      if (i > 0) {
        // Leftmost breadcrumb; don't include 6th vertex.
        points.push(`${tipWidth},${breadcrumbHeight / 2}`);
      }
      console.log({dis: this,points})
      return points.join(" ");
    }
  }
  ngOnInit(): void {
    d3.json("http://localhost:4200/assets/sequence-config.json").then((config: any)=>{
      d3.json("http://localhost:4200/assets/sequence-data.json").then((data: any)=>{
        this.width = config.width
        const breadcrumbWidth = this.breadcrumbWidth = config.breadcrumbWidth
        const breadcrumbHeight = this.breadcrumbHeight = config.breadcrumbHeight
        this.radius = this.width / 2 -50 

        const breadcrumb_svg = d3.select("figure#sunburst-chart-breadcrumb")
        .append("svg")
        .attr("viewBox", `0 0 ${this.breadcrumbWidth * 10} ${this.breadcrumbHeight}`)
        .style("font", "12px sans-serif")
        .style("margin", "5px");
        
        const updateBreadcrumb= (sunburst:any)=>{
          breadcrumb_svg.selectAll("*").remove()
          const g = breadcrumb_svg
          .selectAll("g")
          .data(sunburst.sequence)
          .join("g")
          .attr("transform", (d:any, i:any) => `translate(${i * this.breadcrumbWidth}, 0)`);
      
          g.append("polygon")
            .attr("points", this.getBreadcrumbPoints())
            .attr("fill", (d:any) => color(d.data.name))
            .attr("stroke", "white");
        
          g.append("text")
            .attr("x", (this.breadcrumbWidth + 10) / 2)
            .attr("y", 15)
            .attr("dy", "0.35em")
            .attr("text-anchor", "middle")
            .attr("fill", "white")
            .text((d:any) => d.data.name);
        
          breadcrumb_svg
            .append("text")
            .text(sunburst.percentage > 0 ? sunburst.percentage + "%" : "")
            .attr("x", (sunburst.sequence.length + 0.5) * breadcrumbWidth)
            .attr("y", breadcrumbHeight / 2)
            .attr("dy", "0.35em")
            .attr("text-anchor", "middle");

        }
      
        const arc = d3
          .arc()
          .startAngle((d:any) => d.x0)
          .endAngle((d:any) => d.x1)
          .padAngle(1 / this.radius)
          .padRadius(this.radius)
          .innerRadius((d:any) => Math.sqrt(d.y0))
          .outerRadius((d:any) => Math.sqrt(d.y1) - 1)
        const color = d3
          .scaleOrdinal()
          .domain(["home", "product", "search", "account", "other", "end"])
          .range(["#5d85cf", "#7c6561", "#da7847", "#6fb971", "#9e70cf", "#bbbbbb"]) as any
        const mousearc = d3
          .arc()
          .startAngle((d:any) => d.x0)
          .endAngle((d: any) => d.x1)
          .innerRadius((d: any) => Math.sqrt(d.y0))
          .outerRadius(this.radius)
        data = this.buildHierarchy(data)
        const root = this.partition(data);
        const svg = d3.select("figure#sequence-sunburst-chart").append('svg');

        console.log({svg})
        // Make this into a view, so that the currently hovered sequence is available to the breadcrumb
 

        const label = svg
          .append("text")
          .attr("text-anchor", "middle")
          .attr("fill", "#888")
          .style("visibility", "hidden");

        label
          .append("tspan")
          .attr("class", "percentage")
          .attr("x", 0)
          .attr("y", 0)
          .attr("dy", "-0.1em")
          .attr("font-size", "3em")
          .text("");

        label
          .append("tspan")
          .attr("x", 0)
          .attr("y", 0)
          .attr("dy", "1.5em")
          .text("of visits begin with this sequence");

        svg
          .attr("viewBox", `${-this.radius} ${-this.radius} ${this.width} ${this.width}`)
          .style("max-width", `${this.width}px`)
          .style("font", "12px sans-serif");
        console.log("HERE")
        const path = svg
          .append("g")
          .selectAll("path")
          .data(
            root.descendants().filter((d:any) => (d.depth && (d.x1 - d.x0 > 0.001)))
          )
          .join("path")
          .attr("fill", (d:any):string => color(d.data.name))
          .attr("d", arc as any);
          console.log("Also HERE")
      // console.log({element})
      // element!.value = { sequence: [], percentage: 0.0 };
      svg
        .append("g")
        .attr("fill", "none")
        .attr("pointer-events", "all")
        .on("mouseleave", () => {
          path.attr("fill-opacity", 1);
          label.style("visibility", "hidden");
          // Update the value of this view
          const element = svg.node() as any;
          element.value = { sequence: [], percentage: 0.0 };
          updateBreadcrumb(element.value)
          element.dispatchEvent(new CustomEvent("input"));
        })
        .selectAll("path")
        .data(
          root.descendants().filter((d:any) => {
            // Don't draw the root node, and for efficiency, filter out nodes that would be too small to see
            return d.depth && d.x1 - d.x0 > 0.001;
          })
        )
        .join("path")
        .attr("d", mousearc as any)
        .on("mouseenter", (event:any, d:any) => {
          // Get the ancestors of the current segment, minus the root
          const sequence = d
            .ancestors()
            .reverse()
            .slice(1);
          // Highlight the ancestors
          path.attr("fill-opacity", (node:any) =>
            sequence.indexOf(node) >= 0 ? 1.0 : 0.3
          );
          const percentage = ((100 * d.value) / root.value).toPrecision(3);
          label
            .style("visibility", null)
            .select(".percentage")
            .text(percentage + "%");
          // Update the value of this view with the currently hovered sequence and percentage
          const element = svg.node() as any;
          element.value = { sequence, percentage };
          updateBreadcrumb(element.value)
          element.dispatchEvent(new CustomEvent("input"));
        });

    })
  })
}
}