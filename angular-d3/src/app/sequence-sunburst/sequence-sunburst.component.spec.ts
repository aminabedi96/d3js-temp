import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SequenceSunburstComponent } from './sequence-sunburst.component';

describe('SequenceSunburstComponent', () => {
  let component: SequenceSunburstComponent;
  let fixture: ComponentFixture<SequenceSunburstComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SequenceSunburstComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SequenceSunburstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
