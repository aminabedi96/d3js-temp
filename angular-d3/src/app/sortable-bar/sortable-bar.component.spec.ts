import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SortableBarComponent } from './sortable-bar.component';

describe('SortableBarComponent', () => {
  let component: SortableBarComponent;
  let fixture: ComponentFixture<SortableBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SortableBarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SortableBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
