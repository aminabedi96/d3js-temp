import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3'

type PieData = {
  name: string;
  value: number;
};

@Component({
  selector: 'app-pie',
  templateUrl: './pie.component.html',
  styleUrls: ['./pie.component.css']
})

export class PieComponent implements OnInit {
  private data: PieData[];
  private config: any;
  private svg:any;
  private radius:number;
  // The radius of the pie chart is half the smallest side
  private colors:any;

  constructor() { 
    this.data = []
    this.config = {}
    this.radius = 0
  }

  private createSvg(width:number, height:number, margin: number): void {
    this.radius = Math.min(width, height) / 2 - margin;
    console.log("CREATING")
    // const {width, height} = this.config 
    this.svg = d3.select("figure#pie-chart")
    .append("svg")
    .attr("width", width)
    .attr("height", height)
    .append("g")
    .attr(
      "transform",
      "translate(" + width / 2 + "," + height / 2 + ")"
    );
  }

  private drawChart(): void {
    // Compute the position of each group on the pie:
    const pie = d3.pie<any>().value((d: any) => Number(d.value));

    // Build the pie chart
    this.svg
    .selectAll('pieces')
    .data(pie(this.data))
    .enter()
    .append('path')
    .attr('d', d3.arc()
      .innerRadius(10)
      .outerRadius(this.radius)
    )
    .attr('fill', (d:any, i:any) => (this.colors(i)))
    .attr("stroke", "#121926")
    .style("stroke-width", "1px")


    // Add labels
    const labelLocation = d3.arc()
    .innerRadius(100)
    .outerRadius(this.radius);

    this.svg
    .selectAll('pieces')
    .data(pie(this.data))
    .enter()
    .append('text')
    .text((d:any) => d.data.name)
    .attr("transform", (d:any) => "translate(" + labelLocation.centroid(d) + ")")
    .style("text-anchor", "middle")
    .style("font-size", 15);
    console.log(this.data)
  }

  private createColors(colorRange:string[]): void {
    this.colors = d3.scaleOrdinal()
    .domain(this.data.map(d => d.value.toString()))
    .range(colorRange);
  }

  ngOnInit(): void {
    d3.json("http://localhost:4200/assets/pie-config.json").then((config: any)=>{
      d3.json("http://localhost:4200/assets/pie-data.json").then((data: any)=>{
        this.data = data
        const {width, height, margin, colorRange} = config
        this.createSvg(width, height, margin);
        this.createColors(colorRange);
        this.drawChart();
      })
    })
    
  }

}
