import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SortableBarComponent } from './sortable-bar/sortable-bar.component';
import { PieComponent } from './pie/pie.component';
import { ScatterComponent } from './scatter/scatter.component';
import { SequenceSunburstComponent } from './sequence-sunburst/sequence-sunburst.component';

@NgModule({
  declarations: [
    AppComponent,
    SortableBarComponent,
    PieComponent,
    ScatterComponent,
    SequenceSunburstComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
